<?php

namespace Cpeople\Classes\Forms\Commands;

use Cpeople\Classes\Forms\Command;
use Cpeople\Classes\Forms\Form;

class PopFilesPropUrlCommand extends Command
{
    private $iBlockId;
    private $siteUrl;
    /**
     * @var Form
     */
    private $form;

    public function execute(Form $form)
    {
        $this->form = $form;

        $extraData = $this->popFilesPropUrl();
        foreach($extraData as $key => $val) {
            $form->setDataItem($key, $val);
        }
    }

    public function __construct($isCritical, $iBlockId, $siteUrl)
    {
        parent::__construct($isCritical);

        $this->iBlockId = $iBlockId;
        $this->siteUrl = $siteUrl;
    }

    public function popFilesPropUrl()
    {
        $data = $this->form->getData();
        $elemId = $data['ID'];
        if(!$elemId) throw new \Exception('element id not set');

        $elem = \Cpeople\Classes\Block\Getter::instance()
            ->setFilter(array('IBLOCK_ID' => $this->iBlockId))
            ->getById($elemId);

        $extraData = array();
        foreach (cp_get_ib_properties($this->iBlockId) as $key => $property)
        {
            if ($property['PROPERTY_TYPE'] == 'F' && ($filesId = $elem->getPropValue($key)))
            {
                if(!is_array($filesId)) $filesId = array($filesId);

                foreach($filesId as $fileId) {
                    try {
                        $file = \Cpeople\Classes\Block\File::fromId($fileId);
                        $extraData[$key . '_URL'][] = $this->siteUrl . $file->getUrl();
                    } catch (\Exception $e) {
                    }
                }
            }
        }

        return $extraData;
    }
}