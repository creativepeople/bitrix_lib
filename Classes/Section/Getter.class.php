<?php

namespace Cpeople\Classes\Section;

class Getter extends \Cpeople\Classes\Base\Getter
{
    protected $arFilter = null;
    protected $bIncCnt = false;
    protected $bAddUFToSelect = true;
    protected $className = \Cpeople\Classes\Section\Entity::class;
    
    protected $total;

    /**
     * @static
     * @return Getter
     */
    static function instance()
    {
        return new static();
    }

    /**
     * @return Getter
     */
    public function fetchUserFields($value)
    {
        $this->fetchUserFields = (bool) $value;
        return $this;
    }

    /**
     * @param $pagingSize
     * @param $pageNum
     * @return Getter
     */
    public function paginate($pagingSize, $pageNum)
    {
        $this->setPageSize($pagingSize);
        $this->setPageNum(intval($pageNum) < 1 ? 1 : intval($pageNum));
        return $this;
    }

    /**
     * @return Getter
     */
    public function setIncludeCount($value)
    {
        $this->bIncCnt = (bool) $value;
        return $this;
    }

    /**
     * @return Getter
     */
    public function checkPermissions($value)
    {
        $this->addFilter('CHECK_PERMISSIONS', ($value ? 'Y' : 'N'));
        return $this;
    }

    /**
     * @param $bAddUFToSelect
     * @return $this
     */
    public function setAddUFToSelect($bAddUFToSelect)
    {
        $this->bAddUFToSelect = $bAddUFToSelect;
        return $this;
    }

    /**
     * @return \Cpeople\Classes\Section\Entity[]
     */
    public function get()
    {
        if (\Cpeople\Classes\Registry::bitrixCacheEnabled() && ($retval = $this->getCachedResult()))
        {
            return $retval;
        }

        $retval = array();

        if (!is_array($this->arSelectFields))
        {
            $this->arSelectFields = array();
        }

        if($this->bAddUFToSelect)
        {
            $this->arSelectFields[] = 'UF_*';
        }

        $resultSet = \CIBlockSection::GetList($this->arOrder, $this->arFilter, $this->bIncCnt, $this->arSelectFields, $this->arNavStartParams);

        if (isset($this->resultSetCallback))
        {
            $resultSet = call_user_func($this->resultSetCallback, $resultSet);
        }

        while ($section = $resultSet->GetNext())
        {
            foreach ((array) $this->callbacks as $callback)
            {
                if ($callbackResult = call_user_func($callback, $section))
                {
                    $section = $callbackResult;
                }
            }

            $key = $this->hydrateById ? $section['ID'] : count($retval);

            switch ($this->hydrationMode)
            {
                case self::HYDRATION_MODE_OBJECTS_ARRAY:
                    $className = $this->className;
                    $retval[$key] = new $className($section);
//                    $retval[$key] = new Object($section);
                break;


                default:
                    $retval[$key] = $section;
                break;
            }
        }

        if (\Cpeople\Classes\Registry::bitrixCacheEnabled())
        {
            $this->cacheResult($retval);
        }

        return $retval;
    }
    
    public function getFoundRows()
    {
        $getter = clone $this;

        // в фильтре есть сложная логика, простая группировка не даст нужного
        // результата, получаем все элементы и считаем из
        if (true || array_key_exists(0, $this->arFilter))
        {
            $getter
                ->setNavStartParams(array())
                ->setOrder(array())
                ->setFetchMode(self::FETCH_MODE_FIELDS)
                ->setHydrationMode(self::HYDRATION_MODE_ARRAY)
                ->setSelectFields(array('ID'))
            ;

            if ($this->cacheManager)
            {
                $getter->setCacheManager($this->cacheManager);
            }

            $this->total = (int) count($getter->get());
        }
        else
        {
            $res = $getter
                ->setNavStartParams(null)
                ->setGroupBy(array('IBLOCK_ID'))
                    ->getResult()
                        ->Fetch();

            $this->total = empty($res) ? false : $res['CNT'];
        }

        return $this->total;
    }

    /**
     * @return \paging
     */
    public function getPagingObject($urlTemplate, $total = null)
    {
        if (isset($total))
        {
            $this->total = $total;
        }

        if (!isset($this->total))
        {
            $this->total = $this->getFoundRows();
        }

        $paging = new \Cpeople\paging($this->arNavStartParams['iNumPage'], $this->total, $this->arNavStartParams['nPageSize']);
        $paging->setFormat($urlTemplate);

        return $paging;
    }
}
